#!/bin/sh

echo "Main program is running"
cat /dev/null > output.c
echo '\n' "Please enter the input filename : " 
read ipFilename

while true; do

echo '\n' "Please choose any of the below option " '\n' "1 for Mean Median Mode " '\n' "2 for Range, InterQuarter Range and Standard Deviation " '\n' "3 for Graphs of mean median mode in libreoffice "'\n' "4 for Graphs of range standard devaition in libreoffice "'\n' "X for exit "
read readOption

if  ( [ "$readOption" = "A" ] || [ "$readOption" = "B" ] )
then
	echo -n "Valid entries "
fi

case "$readOption" in
  1)
    echo "Calling MMM"
    ./mmm $ipFilename
    ;;
  2)
    echo "Calling RISC"
    ./ris $ipFilename
    ;;
  3) 
   echo "Calling libreoffice "
   libreoffice --calc /home/mtech/Desktop/jees/rm/risg.ods
    ;;
  4) 
   echo "Calling libreoffice "
   libreoffice --calc /home/mtech/Desktop/jees/rm/mmmg.ods
    ;;
  X)
    echo '\n' "Good Bye "
    break
    ;;
  *)
	echo '\n' "Invalid entries, please try again "
    ;;
esac

#if ( [ "$readOption" <> "X" ] && [ "$readOption" <> "A" ] && [ "$readOption" <> "B" ] )
#then
#	echo '\n' "Invalid entries, please try again "
#fi
done
